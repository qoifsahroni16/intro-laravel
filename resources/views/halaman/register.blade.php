
@extends('layout.master')
@section('judul')
  Registrasi
@endsection
@section ('content')
<body>
<h1>Buat Account Baru</h1>
<h2>Sign up form</h2>
<form action="/welcome"method="POST">
    @csrf
    <p>First Name :</p>
    <input type="text" name ="nama_depan">
    <p>Last Name :</p>
    <input type="text" name = "nama_belakang">
    <p>Gender</p>
    <input type="radio" name="jk">male<br>
    <input type="radio" name="jk">female<br>
    <input type="radio" name="jk">other<br>
    <p>Nationally</p>
    <select name="Nationally">
            <option value="">Indonesia</option>
            <option value="">Inggris</option>
            <option value="">Arab</option>
    </select>
    <p>Language Spoken</p>
    <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
    <input type="checkbox" name="bahasa">Bahasa Inggris<br>
    <input type="checkbox" name="bahasa">Bahasa Arab<br>
    <p>Bio</p>
    <textarea cols="30" rows="10"></textarea><br>
    <input type="submit" value="sign up">
    <h5>@Qoif Sahroni#PKS DIGITAL SCHOOL</h5>  
</form>
</body>
</html>
@endsection