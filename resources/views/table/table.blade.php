@extends('layout.master')
@section('judul')
  Halaman table
@endsection
@section ('content')

<table class="table table-success table-striped table-bordered">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Depan</th>
        <th scope="col">Nama Belakang</th>
        <th scope="col">Hobi</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">001</th>
        <td>Qoif</td>
        <td>Sahroni</td>
        <td>Futsal</td>
      </tr>
      <tr>
        <th scope="row">002</th>
        <td>Ahmad</td>
        <td>Vikri</td>
        <td>Berenang</td>
      </tr>
      <tr>
        <th scope="row">003</th>
        <td>Nuansa</td>
        <td>Ilham</td>
        <td>Berlari</td>
      </tr>
    </tbody>
  </table>
  @endsection